// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  urlHome: 'file:///home/gabriel/Documentos/site-pessoal-desenvolvedor/index.html',
  firebase: {
    apiKey: "AIzaSyC12uOElCqGL0oaMGq0bjTGjt_GkSQ_nmE",
  authDomain: "certificados-portifolio-fc277.firebaseapp.com",
  projectId: "certificados-portifolio-fc277",
  storageBucket: "certificados-portifolio-fc277.appspot.com",
  messagingSenderId: "581826480966",
  appId: "1:581826480966:web:ca21775d168dbca449b347",
  measurementId: "G-BKF0MXWVBN"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
